from PIL import Image
import sys, getopt

#class containg suppoert functions
class PictureEdit:
    #defined operations
    class Operations:
        sharpen = "S"
        identity = "I"
        blur  = "B"
        
    #function getting a mask
    def getMask(operation = Operations.identity):
        if(operation == PictureEdit.Operations.sharpen):
            mask = Sharpen()
        elif (operation == PictureEdit.Operations.identity):
            mask = Identity()
        elif (operation == PictureEdit.Operations.blur):
            mask = Blur()
            
        return mask


    #function for calculated
    def getValue(mask, pixel : int, pixelN = 0, pixelE = 0, pixelS = 0, pixelW = 0, pixelNW = 0, pixelNE = 0, pixelSE = 0, pixelSW = 0):
        value = 0
        
        if(all(el != 0 for el in mask.getSides())):
            for mul,val in zip(mask.getSides(),[pixelN,pixelE,pixelS,pixelW]):
                value += mul*val
                
        if(all(el != 0 for el in mask.getSides())):
            for mul,val in zip(mask.getCorners(),[pixelNW,pixelNE,pixelSE,pixelSW]):
                value += mul*val
                
        value += mask.middle * pixel
        
        return value

# following classes are predefined masks that are applied
class Mask:
    def __init__(self,middle = 0,sides = [0,0,0,0],corners = [0,0,0,0]) -> None:
        self.middle = middle
        self.sides = sides
        self.corners = corners
    
    def getMiddle(self):
        return self.middle
    
    def getCorners(self):
        return self.corners
    
    def getSides(self):
        return self.sides
    
class Sharpen(Mask):  
    def __init__(self) -> None:
        super().__init__(5,[-1,-1,-1,-1])
        
class Identity(Mask):  
    def __init__(self) -> None:
        super().__init__(1)
        
class Blur(Mask):  
    def __init__(self) -> None:
        super().__init__(1/9,[1/9,1/9,1/9,1/9],[1/9,1/9,1/9,1/9])
        
#main function ------------------

def main(argv, operation = PictureEdit.Operations.identity):
    input = "input.png"
    output = "output.png"
    
    #arguments parssing
    try:
        opts, args = getopt.getopt(argv,"hi:o:t:",["ifile=","ofile=","task="])
    except getopt.GetoptError:
        print ('test.py -i <inputfile> -o <outputfile>')
        sys.exit(2)
        
    for opt, arg in opts:
        if opt == '-h':
            print ('test.py -i <inputfile> -o <outputfile> -r <task>')
            print ('tasks:\nS - sharpen\nI - identity\nB - blur')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            input = arg
        elif opt in ("-o", "--ofile"):
            output = arg
        elif opt in ("-t", "--task"):
            if(arg == "S"):
                operation = PictureEdit.Operations.sharpen
            elif (arg == "I"):
                operation = PictureEdit.Operations.identity
            elif (arg == "B"):
                operation = PictureEdit.Operations.blur
                
    #picture edit
    try:
        try:
            img = Image.open(input,"r")
        except:
            print("Could not open the input image!\nExiting...")
            sys.exit(1)
        
        try:    
            #header data
            imgHeight = img.height
            imgWidth = img.width
            imgData = img.getdata() #get pointer to pixel location
            data = list(imgData)    #puct data into list
            newData = [(0,0,0)] * imgHeight * imgWidth #predefines array for new data
            offset = [1,-1,imgWidth,-imgWidth,-imgWidth-1,-imgWidth+1,imgWidth-1,imgWidth+1] #offsets in data from pixel index loaction
            mask = PictureEdit.getMask(operation)
        except:
            print("Could not get all neccery header varibles!\nExiting...")
            exit(1)
        
        #main loop
        for heigth in range(imgHeight):
            for width in range(imgWidth):
                index = heigth * imgWidth + width #index in data
                if(heigth > 0 and width > 0 and heigth < imgHeight-1 and width < imgWidth-1): #checks if pixel is not border pixel
                    valueM = data[index]
                    values = [0,0,0,0,0,0,0,0]
                    ix = 0
                    for i in offset:
                        values[ix] = data[index + i]
                        ix += 1
                        
                    pixel = [0] * len(valueM)
                    
                    #goes through all pixel numbers
                    for i in range(len(valueM)):
                        #claculates new pixel color values
                        value = PictureEdit.getValue(mask,valueM[i],
                                                        values[0][i],values[1][i],values[2][i],values[3][i],
                                                        values[4][i],values[5][i],values[6][i],values[7][i])
                        pixel[i] = value
                        
                    value = (int(pixel[0]),int(pixel[1]),int(pixel[2]))
                else:   # just copies the pixel
                    value = data[index]
                    
                newData[index] = value

            #progress calculation
            if ((heigth+1) % int(imgHeight/10)) == 0:
                per = heigth/imgHeight
                per *= 100
                per = round(per)
                print(str(per) + "%")
        
        #img saveing
        try:
            img.putdata(newData)
            img.save(output)
        except:
            print("could not save the image")
            
        try:
            img.close()
        except:
            print("could not close the input file")
            sys.exit(1)
    except:
        #exepictio if soemthing went wrong
        print("Error has occured")
        print("Try checking the arguments passed and rerun the program")
        print("For agrument information start the program with -h parametr")
        exit(1)


#main ---------------------

if(__name__ == '__main__'):
    print("\nStarting the program --------- \n")
    main(sys.argv[1:])
    print("\nEnding the program ----------- \n")
